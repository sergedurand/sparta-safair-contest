from pathlib import Path
from environment_setup import PROJ_DIR
import sys
from networks.models.AbstractNetwork import Network
from torch import nn
import torch
import torchvision.models as models
import os

MODELS = ["resnet18","resnet34","resnet50","resnet101","resnet152","w_resnet50","w_resnet101","resnext50","resnext101"]

class Resnet(nn.Module,Network):
    def __init__(self, name, num_classes=40,model="resnet18") -> None:
        super().__init__()
        super(nn.Module,self).__init__(name=name)
        self.num_classes = num_classes
        if model == "resnet18":
            self.backbone = models.resnet18(pretrained=True)
        elif model == "resnet34":
            self.backbone = models.resnet34(pretrained=True)
        elif model == "resnet50":
            self.backbone = models.resnet50(pretrained=True)
        elif model == "resnet101":
            self.backbone = models.resnet101(pretrained=True)
        elif model == "resnet152":
            self.backbone = models.resnet152(pretrained=True)
        elif model == "w_resnet50":
            self.backbone = models.wide_resnet50_2(pretrained=True)
        elif model == "w_resnet101":
            self.backbone = models.wide_resnet101_2(pretrained=True)
        elif model == "resnext50":
            self.backbone = models.resnext50_32x4d(pretrained=True)
        elif model == "resnext101":
            self.backbone = models.resnext101_32x8d(pretrained=True)
        else:
            raise NotImplementedError("Only supportint {}".format(MODELS))
        child_counter = 0
        for child in self.backbone.children():
            if child_counter < 8:
                for param in child.parameters():
                    param.requires_grad = False
            # elif child_counter == 6:
            #     children_of_child_counter = 0
            #     for children_of_child in child.children():
            #         if children_of_child_counter == 0:
            #             for param in children_of_child.parameters():
            #                 param.requires_grad = False
            #         children_of_child_counter += 1
            child_counter += 1
        self.classifier = nn.Linear(1000, self.num_classes)

    def forward(self,x):
        y = self.backbone(x)
        return self.classifier(y)

    def save(self, model_number, checkpoint_dir):
        """
        Utility function to save the model
        :param index: The counter added to model name to distinguish different models
        :param checkpoint_dir: Location where to save the model
        :return: None
        """
        # Store the model snapshot
        filename = self.name + '{:d}'.format(model_number) + '.pth'
        filename = Path(checkpoint_dir) / filename
        if not Path(checkpoint_dir).is_dir():
            Path(checkpoint_dir).mkdir()
        torch.save(self.state_dict(), str(filename))
        print('Saving snapshot to {}'.format(filename))

    def load(self, model_number, checkpoint_dir):
        """
        Utility function to load the model
        :param model_number: Specific counter added to model name to distinguish different models
        :param checkpoint_dir: Location from where to load the model
        :return: None
        """
        filename = self.name + '{:d}'.format(model_number) + '.pth'
        s_file = os.path.join(checkpoint_dir, filename)
        print('Restoring mode snapshots from {:s}'.format(filename))
        self.load_state_dict(torch.load(s_file))
        print('Restored')


def check_model_size(model):
    """
    Utility function to check the number of parameters in the model. Prints the value in Millions
    :param model: Input model to be checked for size
    :return: None
    """
    num_params = 0
    traininable_param = 0
    for param in model.parameters():
        num_params += param.numel()
        if param.requires_grad:
            traininable_param += param.numel()
    print("[Network  Total number of parameters : %.3f M" % (num_params / 1e6))
    print(
        "[Network  Total number of trainable parameters : %.3f M"
        % (traininable_param / 1e6)
    )

if __name__ == "__main__":

    for model in MODELS:
        net = Resnet(name="res",model=model)
        print("{}".format(model),end=", ")
        print(len(list(net.backbone.children())))
        check_model_size(net)