import torch
from pathlib import Path
import sys
sys.path.append(str(Path(__file__).parent.parent.parent))
from attacks.attack_models.PGD import PGD
from attacks.attack_models.CarliniWagnerL2Attack import CarliniWagnerL2Attack
from networks.models.AutoEncoderDefenceNetwork import AutoEncoderDefenceNetwork

from defence.defence_models.base import AbstractDefence
from networks import DummyNetwork, models


class AutoEncoderAdversarialDefence(AbstractDefence):

    def __init__(self, attack_method, model=None):
        super(AutoEncoderAdversarialDefence, self).__init__()
        self.attack_method = attack_method
        self.model = model

    def forward(self, model, images, labels):
        """
        :param model: The model being passed
        :param images: Images
        :param labels: Corresponding labels
        :return: perturbed sample
        """
        # generate perturbed samples for feeding into the model
        noisy_input = self.attack_method.generate_perturbed_image(images, labels, train=True)
        print(noisy_input.device)
        output = model(noisy_input, images)
        return output

    @property
    def is_perturbed(self):
        """
        Property to indicate if we are processing perturbed samples or not. Since `AutoEncoderAdversarialDefence` is using
        perturbations, value is True. The function helps duretwork =ng Adversarial training to print properly formatted
        loss and accuracy metric.
        :return: True
        """
        return True

    def additional_defence_loss(self, *args, **kwargs):
        """
        Scalar value to add any auxiliary loss computed during training. Specifically used in case of AutoEncoder based
        defence in the samples.
        :return: Scalar value to be added to the loss
        """
        return self.model.additional_loss


if __name__ == '__main__':
    model = AutoEncoderDefenceNetwork(name="sample").cuda()
    
    attack = CarliniWagnerL2Attack(model=model,save_folder=None)
    defence_network = AutoEncoderAdversarialDefence(attack_method=attack,model=model)
    x = torch.randn((10, 3, 256, 256)).cuda()  # B, num_channel, H, W
    output = defence_network(model, x, None)
    print(output.shape)
    print(defence_network.additional_defence_loss())
