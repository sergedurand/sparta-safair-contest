from dataset.AttributeAlterationDataset import get_data_loader
from dataset.utils import CelebA
from environment_setup import PROJ_DIR

dataloader = get_data_loader(4, 'valid')
dataset = dataloader.dataset
for img_name, img, label in dataloader:
    print(img_name)
    print(img.shape)
    print(img.max())
    print(img.min())
    print(label.shape)
    # print(dataloader.dataset.pred_acc)
    break
