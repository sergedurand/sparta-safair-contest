#!/bin/bash
# Install the python packages from requirements.txt
# Uses mamba by default and pip if mamba couldn't find package
# Put script in same folder as requirements.txt (or change path here)
# Conda version: just replace mamba by conda
export ftp_proxy=ftp://proxy.ufr-info-p6.jussieu.fr:3128 && export https_proxy=http://proxy.ufr-info-p6.jussieu.fr:3128 && export use_proxy=yes && git config --global http.proxy http://proxy.ufr-info-p6.jussieu.fr:3128 && git config --global https.proxy https://proxy.ufr-info-p6.jussieu.fr:3128 && export TORCH_HOME="/tempory/cache/checkpoints"

while read requirement; do mamba install --yes -c conda-forge $requirement || pip install $requirement; done < requirements.txt
